const socket = io("/");
const myPeer = new Peer(undefined, {
  host: "peerjs.externall.ink",
  //port: "80",
  path: "/myapp",
});
const videoGrid = document.getElementById("video-grid");
const myVideo = document.createElement("video");
myVideo.muted = true;

let peers = {};

navigator.mediaDevices
  .getUserMedia({
    video: true,
    audio: true,
  })
  .then((stream) => {
    addVideoStream(myVideo, stream);
    console.log(myPeer);
    myPeer.on("call", (call) => {
      call.answer(stream);
      const video = document.createElement("video");
      video.classList.add("peers");
      call.on("stream", (userVideoStream) => {
        addVideoStream(video, userVideoStream);
      });
      call.on("close", () => {
        video.remove();
      });
      peers[call.peer] = call;
    });

    socket.on("user-connected", (userID) => {
      connectToNewUser(userID, stream);
    });
  });

socket.on("user-disconnected", (userID) => {
  if (peers[userID]) peers[userID].close();
});

myPeer.on("open", (id) => {
  socket.emit("join-room", ROOM_ID, id);
});

function addVideoStream(video, stream) {
  video.srcObject = stream;
  video.addEventListener("loadedmetadata", () => {
    video.play();
  });
  videoGrid.append(video);
}

function connectToNewUser(userID, stream) {
  const call = myPeer.call(userID, stream);
  const video = document.createElement("video");
  video.classList.add("peers");
  call.on("stream", (userVideoStream) => {
    addVideoStream(video, userVideoStream);
  });
  call.on("close", () => {
    video.remove();
  });

  peers[userID] = call;
}
